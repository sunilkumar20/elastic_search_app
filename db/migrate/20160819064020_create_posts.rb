class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.belongs_to :article
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
