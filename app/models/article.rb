require 'elasticsearch/model'
# require 'elasticsearch'
# require 'elasticsearch/persistence'
class Article < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  extend ActiveSupport::Concern
  has_many :posts

  # index_name    "articles-#{Rails.env}"
  # document_type "post"
  # index_name Rails.application.class.parent_name.underscore
  # document_type self.name.downcase

  

  # settings index: { number_of_shards: 1, number_of_replicas: 0 } do
  #   mapping dynamic: false do 

  #     indexes :id, type: 'long'
  #     indexes :title, type: 'string'
  #     indexes :description, type: 'string'
  #     # indexes :family_id, type: 'long'
  #     # indexes :collection_id, type: 'long'
  #     # indexes :created_at, type: 'date'
  #     # indexes :updated_at, type: 'date'

  #     indexes :posts, type: 'nested' do
  #       indexes :id, type: 'long'
  #       indexes :title, type: 'string'
  #     end

  #     # indexes :categories, type: 'nested' do
  #     #   indexes :id, type: 'long'
  #     #   indexes :name, type: 'string'
  #     # end

  #   end
  # end

  # def as_indexed_json
  #   self.as_json({
  #     only: [:title, :description],
  #     include: {
  #       posts: { only: :title },
  #     }
  #   })
  # end


  # settings index: { number_of_shards: 1 } do
  #   mappings dynamic: 'false' do
  #     indexes :title, analyzer: 'english', index_options: 'offsets'
  #   end
  # end

  def as_indexed_json(options={})
    as_json({
      only: ['title','created_at', 'description'],
      methods: [],
      include: {
        posts: {only: [:title, :description] }
      }   
    })  
  end 

   mapping _source: { excludes: [] } do
    indexes :id, type: 'integer'
    indexes :title
  end

  # Article.search('*foo*').results.as_json
  def self.search(query, post_q = '')
    fields_data = nil
    if query.present?
      fields_data = ["title"]
      query_input = query
    elsif post_q.present?
      fields_data = ["posts.title"]
      query_input = post_q
    else
      fields_data = ["title"]
      query_input = query
    end

    __elasticsearch__.search(
      # {
      #   query: {
      #     multi_match: {
      #       query: query,
      #       fields: ['title', 'description', "posts.title"]
      #     }
      #   },
      #   highlight: {
      #     pre_tags: ['<em>'],
      #     post_tags: ['</em>'],
      #     fields: {
      #       title: {},
      #       text: {}
      #     }
      #   }
      # }
      {:query=>{
        :filtered=>{
          :query=>{
            :query_string=>{
              :query=> query_input, 
              :fields=>fields_data
            }
          }, 
          :filter=>{
            :bool=>{
              :should=>[
                { 
                  :terms=>{"posts.title"=> [post_q] } 
                },
                :range=>{
                  :created_at=>{
                    :gt=>"now-1w"
                  }
                }
              ] 
              # :should=>[]
            }
            }
          }
        }
      }
    )
  
end


#  def self.search(query)
#   __elasticsearch__.search(
#     {
#       query: {
#         multi_match: {
#           query: query,
#           fields: ['title']
#         }
#       },
#       highlight: {
#         pre_tags: ['<em>'],
#         post_tags: ['</em>'],
#         fields: {
#           title: {},
#           text: {}
#         }
#       }
#     }
#   )
# end

    # settings index: { number_of_shards: 1, number_of_replicas: 0 } do
    #   mapping do

    #     indexes :id, type: 'long'
    #     indexes :title, type: 'string'
    #     indexes :description, type: 'string'
    #     # indexes :family_id, type: 'long'
    #     # indexes :collection_id, type: 'long'
    #     # indexes :created_at, type: 'date'
    #     # indexes :updated_at, type: 'date'

    #     indexes :posts, type: 'nested' do
    #       indexes :id, type: 'long'
    #       indexes :title, type: 'string'
    #     end

    #     # indexes :categories, type: 'nested' do
    #     #   indexes :id, type: 'long'
    #     #   indexes :name, type: 'string'
    #     # end

    #   end
    # end

  #   def self.search(options={})
  #     __set_filters = lambda do |key, f|

  #       @search_definition[:filter][:and] ||= []
  #       @search_definition[:filter][:and]  |= [f]
  #     end

  #     @search_definition = {
  #       query: {
  #         filtered: {
  #           query: {
  #             match_all: {}
  #           }
  #         }
  #       },
  #       filter: {}
  #     }

  #     if options[:article]
  #       f = { term: { "title": options[:article] } }

  #       __set_filters.(:title, f)
  #       __set_filters.(:description, f)
  #       # __set_filters.(:categories, f)
  #     end

  #     def as_indexed_json(options={})
  #       as_json(
  #         include: {:posts => { :only => [ :id, :title ] }
  #       )
  #     end

  #     # if options[:categories]
  #     #   ...
  #     # end

  #     # if options[:collection_id]
  #     #   ...
  #     # end

  #     # if options[:family_id]
  #     #   ...
  #     # end

  #     __elasticsearch__.search(@search_definition)
  #   end

  # end
end
